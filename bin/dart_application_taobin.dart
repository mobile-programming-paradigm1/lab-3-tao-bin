import 'package:dart_application_taobin/dart_application_taobin.dart'
    as dart_application_taobin;
import 'dart:io';

void main(List<String> arguments) {
  String name = ' ';
  String sweet = ' ';
  String value = ' ';
  showCommandsmessage();
  String? type = stdin.readLineSync();
  selectMenu(type, name, value, sweet);
  print(" ");
  print("==================================================================");
  selectPayment();
}

class taobinCommands {
  String name;
  String sweet;
  String value;

  taobinCommands(this.name, this.sweet, this.value);

  void showResult() {
    print("==================================================================");
    stdout.write('Your drink is : ' + name + " --> " + sweet);
    stdout.write(" --> " + value + ' baht');
  }

  String getPrice() {
    return value;
  }
}

void showCommandsmessage() {
  String message = '''
  Select Commands Message
  1. Recommend
  2. Coffee
  3. Tea
  4. Milk Cocoa and Caramel
  5. Protein Shake
  6. Soda and Others
  7. FRUITY DRINKS
  Your choice : ''';
  stdout.write(message);
}

void selectMenu(String? type, String name, String value, String sweet) {
  if (type == '1') {
    String message = '''
   🐢  Menu Recommend 🐢
  1. Iced Tea
  2. Iced Latte
  3. Hot Espresso

Select number of menu : ''';
    stdout.write(message);

    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Iced Tea';
      value = '40';
    } else if (name == '2') {
      name = 'Iced Latte';
      value = '40';
    } else if (name == '3') {
      name = 'Hot Espresso';
      value = '35';
    }
    showSweetLevel();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    taobinCommands drink = new taobinCommands(name, sweet, value);
    drink.showResult();
  } else if (type == '2') {
    String message = '''
   🐢 Coffee Menu 🐢
  1. Iced Mocha
  2. Iced Latte
  3. Hot Espresso

Select number of menu : ''';
    stdout.write(message);
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Iced Mocha';
      value = '40';
    } else if (name == '2') {
      name = 'Iced Latte';
      value = '40';
    } else if (name == '3') {
      name = 'Hot Espresso';
      value = '35';
    }

    showSweetLevel();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    taobinCommands drink = new taobinCommands(name, sweet, value);
    drink.showResult();
  } else if (type == '3') {
    String message = '''
   🐢 Tea Menu 🐢
  1. Iced Tea
  2. Iced Tea Lemon
  3. Hot Tea

Select number of menu : ''';
    stdout.write(message);
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'Iced Tea';
      value = '40';
    } else if (name == '2') {
      name = 'Iced Tea Lemon';
      value = '40';
    } else if (name == '3') {
      name = 'Hot Tea';
      value = '35';
    }

    showSweetLevel();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    taobinCommands drink = new taobinCommands(name, sweet, value);
    drink.showResult();
  } else if (type == '4') {
    String message = '''
   🐢 Milk Cocoa and Caramel Menu 🐢
  1. HOT CARAMEL MILK
  2. ICED CARAMEL MILK
  3. ICED COCOA

Select number of menu : ''';
    stdout.write(message);
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'HOT CARAMEL MILK';
      value = '40';
    } else if (name == '2') {
      name = 'ICED CARAMEL MILK';
      value = '40';
    } else if (name == '3') {
      name = 'ICED COCOA';
      value = '35';
    }

    showSweetLevel();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    taobinCommands drink = new taobinCommands(name, sweet, value);
    drink.showResult();
  } else if (type == '5') {
    String message = '''
   🐢 Protein Shake Menu 🐢
  1. MATCHA PROTEIN SHAKE
  2. CHOCOLATE PROTEIN SHAKE
  3. STRAWBERRY PROTEIN SHAKE
  4. ESPRESSO PROTEIN SHAKE
  5. THAI TEA PROTEIN SHAKE
  6. TAIWANESE TEA PROTEIN SHAKE
  7. CARAMEL PROTEIN SHAKE
  8. PLAIN PROTEIN SHAKE
  9. MILK SHAKE

Select number of menu : ''';
    stdout.write(message);
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'MATCHA PROTEIN SHAKE';
      value = '40';
    } else if (name == '2') {
      name = 'CHOCOLATE PROTEIN SHAKE';
      value = '40';
    } else if (name == '3') {
      name = 'STRAWBERRY PROTEIN SHAKE';
      value = '40';
    } else if (name == '4') {
      name = 'ESPRESSO PROTEIN SHAKE';
      value = '45';
    } else if (name == '5') {
      name = 'THAI TEA PROTEIN SHAKE';
      value = '45';
    } else if (name == '6') {
      name = 'TAIWANESE TEA PROTEIN SHAKE';
      value = '50';
    } else if (name == '7') {
      name = 'CARAMEL PROTEIN SHAKE';
      value = '50';
    } else if (name == '8') {
      name = 'PLAIN PROTEIN SHAKE';
      value = '45';
    } else if (name == '9') {
      name = 'MILK SHAKE';
      value = '45';
    }

    showSweetLevel();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    taobinCommands drink = new taobinCommands(name, sweet, value);
    drink.showResult();
  } else if (type == '6') {
    String message = '''
  🐢 Soda and Others Menu 🐢
 1. ICED LIME SALA SODA 
 2. PEPSI
 3. ICED LIMENADE SODA
 4. ICED LYCHEE SODA
 5. ICED STRAWBERRY SODA
 6. ICED PLUM SODA
 7. ICED GINGER SODA
 8. ICED BLUEBERRY
 9. ICED SALA SODA

Select number of menu : ''';
    stdout.write(message);
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'ICED LIME SALA SODA ';
      value = '40';
    } else if (name == '2') {
      name = 'PEPSI';
      value = '30';
    } else if (name == '3') {
      name = 'ICED LIMENADE SODA';
      value = '35';
    } else if (name == '3') {
      name = 'ICED LIMENADE SODA';
      value = '35';
    } else if (name == '4') {
      name = 'ICED LYCHEE SODA';
      value = '35';
    } else if (name == '5') {
      name = 'ICED STRAWBERRY SODA';
      value = '35';
    } else if (name == '6') {
      name = 'ICED PLUM SODA';
      value = '35';
    } else if (name == '7') {
      name = 'ICED GINGER SODA';
      value = '35';
    } else if (name == '8') {
      name = 'ICED BLUEBERRY';
      value = '35';
    } else if (name == '9') {
      name = 'ICED SALA SODA';
      value = '35';
    }

    taobinCommands drink = new taobinCommands(name, sweet, value);
    drink.showResult();
  } else if (type == '7') {
    String message = '''
  🐢 FRUITY DRINKS Menu 🐢
  1. HOT LIMEADE
  2. ICED LIMEADE
  3. ICED LYCHEE
  4. ICED STRAWBERRY ICED BLUEBERRY
  5. ICED PLUM

Select number of menu : ''';
    stdout.write(message);
    name = stdin.readLineSync()!;
    if (name == '1') {
      name = 'HOT LIMEADE';
      value = '40';
    } else if (name == '2') {
      name = 'ICED LIMEADE';
      value = '40';
    } else if (name == '3') {
      name = 'ICED LYCHEE';
      value = '35';
    } else if (name == '4') {
      name = 'ICED STRAWBERRY ICED BLUEBERRY';
      value = '35';
    } else if (name == '5') {
      name = 'ICED PLUM';
      value = '35';
    }

    showSweetLevel();
    sweet = stdin.readLineSync()!;
    sweet = sweetLevel(sweet);

    taobinCommands drink = new taobinCommands(name, sweet, value);
    drink.showResult();
  }
}

void showSweetLevel() {
  String message = '''
  🐢 Sweet Level 🐢
  1. no sugar added
  2. little sweet
  3. sweet fit
  4. so sweet
  5. sweet 3 worlds

Select number of sweet level : ''';
  stdout.write(message);
}

String sweetLevel(String sweet) {
  if (sweet == '1') {
    sweet = 'no sugar added';
  } else if (sweet == '2') {
    sweet = 'little sweet';
  } else if (sweet == '3') {
    sweet = 'sweet fit';
  } else if (sweet == '4') {
    sweet = 'so sweet';
  } else if (sweet == '5') {
    sweet = 'sweet 3 worlds';
  }
  return sweet;
}

void selectPayment() {
  String message = '''
  🐢 Payment 🐢
  1. Cash
  2. Pay TrueMoney Wallet
  3. Scan QR to pay
  4. Use coupons/view accumulated coupons

Select number of Payment : ''';
  stdout.write(message);
  String? payment = stdin.readLineSync();
  if (payment == '1') {
    print('Cash');
  } else if (payment == '2') {
    print('Pay TrueMoney Wallet');
  } else if (payment == '3') {
    print('Scan QR to pay');
  } else if (payment == '4') {
    print('Use coupons/view accumulated coupons');
  }
}
